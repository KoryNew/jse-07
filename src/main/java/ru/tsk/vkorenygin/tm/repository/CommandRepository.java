package ru.tsk.vkorenygin.tm.repository;

import ru.tsk.vkorenygin.tm.constant.ArgumentConst;
import ru.tsk.vkorenygin.tm.constant.CommandConst;
import ru.tsk.vkorenygin.tm.model.Command;

public class CommandRepository {

	public static final Command ABOUT = new Command(
			CommandConst.ABOUT, ArgumentConst.ABOUT, "displays developer info"
	);

	public static final Command VERSION = new Command(
			CommandConst.VERSION, ArgumentConst.VERSION, "displays program version"
	);

	public static final Command HELP = new Command(
			CommandConst.HELP, ArgumentConst.HELP, "displays list of commands"
	);

	public static final Command INFO = new Command(
			CommandConst.INFO, ArgumentConst.INFO, "displays system information"
	);

	public static final Command EXIT = new Command(
			CommandConst.EXIT, null, "closes the application"
	);

	public static final Command[] commands = new Command[]{
			ABOUT, VERSION, HELP, INFO, EXIT
	};

	public static Command[] getCommands() {
		return commands;
	}

}
